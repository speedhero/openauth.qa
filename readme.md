本项目原本为[Openauth.Net](http://git.oschina.net/yubaolee/OpenAuth.Net/)的配套社区，本着开源的精神，现在开放源码供喜欢的同学一起优化。Openauth.net Star 1200+的质量保证。

openauth.qa是一个简洁实用的问答网站，当然，也可以作为社区使用。官方演示地址：[http://www.openauth.me/club/index
](http://www.openauth.me/club/index)
功能：

    提问、回答及采纳一个都不能少；
    上传、表情及消息提示也是不可或缺；
    设置精华、置顶及积分怎能落下；

特点：

    作为一个拥有多年领域驱动开发的程序员，代码拥有极高的可读性；
    前后端完全分离，前端采用layui的laytpl引擎，后端maven+spring mvc + mybatis；

适用人群：

    急需搭建问答系统或社区系统的开发者；
    初学JAVA EE的新手；

快速预览：

![输入图片说明](http://git.oschina.net/uploads/images/2017/0214/142742_1461ee99_362401.png "在这里输入图片标题")